package com.jdriven.reactive.service;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.jdriven.reactive.domain.Coordinate;

import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;

/**
 * Our World Service. This represents methods for the world our Mememons live in.
 */
@Service
public class WorldService {

    private static final Random RANDOM = new Random();
    private static final Object SIGNAL = new Object();

    private MememonService mememonService;

    private EmitterProcessor<Object> encountersFlux;

    @Autowired
    public WorldService(MememonService mememonService) {
        this.mememonService = mememonService;
        this.encountersFlux = EmitterProcessor.create();
    }

    /**
     * The returning Flux should publish the ID of a Mememon, whenever the below {@link #triggerMememonEncounter()}
     * method is called.
     *
     * Tip: use the {@link MememonService#getAllMememons()} method to get a Flux of all Mememons, and
     * combine (zip) this Flux with the {@link #encountersFlux}. The {@link #encountersFlux} publishes
     * an object everytime a mememon is encountered.
     *
     * @return The Flux of Mememon IDs
     */
    public Flux<Long> getEncounters() {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    public void locationReceived(@SuppressWarnings("unused") Coordinate location) {
        if (RANDOM.nextInt(1000) < 5) {
            triggerMememonEncounter();
        }
    }

    private void triggerMememonEncounter() {
        encountersFlux.onNext(SIGNAL);
    }

}
