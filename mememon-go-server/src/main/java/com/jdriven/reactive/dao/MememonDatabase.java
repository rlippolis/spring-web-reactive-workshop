package com.jdriven.reactive.dao;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import com.google.common.collect.ImmutableSet;
import com.jdriven.reactive.domain.Mememon;

/**
 * Bad implementation of a non-reactive database. Move along, nothing to see here!
 */
final class MememonDatabase {

    private static final AtomicLong ID_GENERATOR = new AtomicLong(1L);
    private static final Random RANDOM = new Random();

    private static final Set<Mememon> MEMEMONS;
    static {
        MEMEMONS = ImmutableSet.of(
                createMememon("Bad Luck Brian", "badluckbrian", 1), // Bad Luck Brian gets caught easily
                createMememon("Epic Win", "epicwin"),
                createMememon("Jackie Chan - Why?!", "jackiewhy"),
                createMememon("LOL Face", "lolface"),
                createMememon("Mouth Rainbow", "mouthrainbow"),
                createMememon("Okay Face", "okay"),
                createMememon("Rage Face", "rageface"),
                createMememon("Troll Face", "trollface"),
                createMememon("Very Bad Face", "verybadface"),
                createMememon("Nicholas Cage - You don't say?!", "youdontsay"),
                createMememon("One does not simply finish mememon", "onedoesnotsimply", 100)
        );
    }

    public static Set<Mememon> getAll() {
        return MEMEMONS;
    }

    private static Mememon createMememon(String name, String imageId) {
        return createMememon(name, imageId, getRandomStrength());
    }

    private static Mememon createMememon(String name, String imageId, int strength) {
        return new Mememon(ID_GENERATOR.getAndIncrement(), name, imageId, strength);
    }

    private static int getRandomStrength() {
        return RANDOM.nextInt(99) + 1;
    }
}
