package com.jdriven.reactive.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TodoList {

    private String name;
    private List<TodoItem> items;

    public TodoList(String name, List<TodoItem> items) {
        this.name = name;
        this.items = new ArrayList<>(items);
    }

    public String getName() {
        return name;
    }

    public List<TodoItem> getItems() {
        return Collections.unmodifiableList(items);
    }
}
