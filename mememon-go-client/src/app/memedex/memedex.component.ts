import {Component, OnInit, SkipSelf} from '@angular/core';
import {Mememon} from "../shared/mememon";
import {List} from "immutable";
import {MememonsService} from "../shared/mememons.service";

@Component({
  selector: 'app-memedex',
  templateUrl: './memedex.component.html',
  styleUrls: ['./memedex.component.css'],
  providers: [MememonsService]
})
export class MemedexComponent implements OnInit {

  private memedex: List<Mememon> = List<Mememon>();

  constructor(@SkipSelf() private mememonsService: MememonsService) { }

  ngOnInit() {
    this.mememonsService.mememonsObservable.subscribe(
      (mememonList: List<Mememon>) => {
        this.memedex = mememonList;
      }
    );
  }

  get mememonsCaught(): List<Mememon> {
    return this.memedex;
  }

}
