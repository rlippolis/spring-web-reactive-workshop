package com.jdriven.reactive.exercises;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;


import com.jdriven.reactive.domain.TodoItem;
import com.jdriven.reactive.domain.TodoList;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

/**
 * Transform the items emitted by a Flux.
 */
public class E3_TransformTest {

    /**
     * Use the `reduce` method to retrieve the maximum Integer in the provided [ints] list.
     *
     * @param ints List of Integer
     * @return Mono of Integer
     */
    public Mono<Integer> max(final List<Integer> ints) {
        return Mono.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Transform the incoming parameter [name] with a prefix of "Hello "
     *
     * @param name String your name
     * @return Flux of String "Hello " + name
     */
    public Flux<String> map(final String name) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Filter the incoming Integers to return only the even numbers and append the text " (even)" to those Integers.
     *
     * @param ints Flux of Integer
     * @return Flux of even Integer
     */
    public Flux<String> filterMap(final List<Integer> ints) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Concatenate the incoming {@link TodoList}s and map the resulting list of TodoItems to their description.
     *
     * HINT: use the `concatMap` method.
     *
     * @param todoLists Flux of {@link TodoList}s
     * @return Flux of String
     */
    public Flux<String> concatMap(final Flux<TodoList> todoLists) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Basically the same as `concatMap`, but using preferred method `flatMap`. In flux streams it is almost always
     * flatMap that should be used: `flatMap` uses merge instead of concat and allows multiple concurrent streams.
     *
     * @param todoLists Flux of {@link TodoList}s
     * @return Flux of String
     */
    public Flux<String> flatMap(final Flux<TodoList> todoLists) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }

    /**
     * Use `zipWith` to merge two Fluxes of String into one.
     *
     * a -> "Daenerys Targaryen", "Tyrion Lannister", "Jon Snow"
     * b -> "Mother of Dragons", "The Imp", "knows nothing"
     * return -> "Daenerys Targaryen, Mother of Dragons", "Tyrion Lannister, The Imp", "Jon Snow, knows nothing"
     *
     * @param a Flux of String
     * @param b Flux of String
     * @return Flux of String
     */
    public Flux<String> zip(final Flux<String> a, final Flux<String> b) {
        return Flux.error(new RuntimeException("TODO: implement me!"));
    }


    //
    // -------------------------------------------------------------------
    //


    @Test
    public void maxTest() {
        StepVerifier.create(max(Arrays.asList(3, 6, 8, 9, 4, 12, 4, 2)))
                .expectNext(12)
                .verifyComplete();
    }

    @Test
    public void mapTest() {
        StepVerifier.create(map("JDriven"))
                .expectNext("Hello JDriven")
                .verifyComplete();
    }

    @Test
    public void filterMapTest() {
        StepVerifier.create(filterMap(IntStream.range(1, 11).boxed().collect(Collectors.toList())))
                .expectNext("2 (even)", "4 (even)", "6 (even)", "8 (even)", "10 (even)")
                .verifyComplete();
    }

    @Test
    public void concatMapTest() {
        StepVerifier.create(concatMap(todoLists))
                .expectNext("Milk", "Eggs", "Cheese", "Sharknado", "The Godfather")
                .verifyComplete();
    }

    @Test
    public void flatMapTest() {
        StepVerifier.create(flatMap(todoLists))
                .expectNext("Milk", "Eggs", "Cheese", "Sharknado", "The Godfather")
                .verifyComplete();
    }

    @Test
    public void zipTest() {
        final Flux<String> zipped = zip(
                Flux.just("Daenerys Targaryen", "Tyrion Lannister", "Jon Snow"),
                Flux.just("Mother of Dragons", "The Imp", "knows nothing")
        );
        StepVerifier.create(zipped)
                .expectNext("Daenerys Targaryen, Mother of Dragons", "Tyrion Lannister, The Imp", "Jon Snow, knows nothing")
                .verifyComplete();
    }

    private final Flux<TodoList> todoLists = Flux.just(
            new TodoList("Groceries", Arrays.asList(
                    new TodoItem("Milk", 1),
                    new TodoItem("Eggs", 2),
                    new TodoItem("Cheese", 3)
            )),
            new TodoList("Movies", Arrays.asList(
                    new TodoItem("Sharknado", 1),
                    new TodoItem("The Godfather", 2)
            ))
    );

}
